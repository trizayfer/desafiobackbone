Router = Backbone.Router.extend({
	routes : {
		'' : 'entrouNaApp',
		'pessoa' : 'formNovaPessoa',
		'pessoa/:id' : 'formEditaPessoa',
		'pessoas' : 'pageListaPessoa',
		'animal/:id' : 'formEditaAnimal',
		'animal' : 'formNovoAnimal',
		'animais' : 'pageListaAnimal'
	},
	
	entrouNaApp : function() {
		console.log('Consegui acessar a app');
	},

	formNovaPessoa : function() {
		var formPessoa = new FormPessoa();
		$('.main-content').html(formPessoa.render().el);
	},
	
	formNovoAnimal : function() {
		var formAnimal = new FormAnimal();
		$('.main-content').html(formAnimal.render().el);
	},
	
	/*formEditaPessoa : function(id) {
		var formPessoa = new FormPessoa({
			model : pageListaPessoa.pessoa.get(),
		});
		$('.main-content').html(formAnimal.render().el);
	},*/

	pageListaPessoa : function() {
		var listaDePessoas = new PessoaCollection();
		listaDePessoas.fetch({
			success : function() {
				var pagePessoas = new TabelaPessoas({
					collection : listaDePessoas
				});
				$('.main-content').html(pagePessoas.render().el);
			},
			error : function() {
				console.error('Nao foi possivel carregar as pessoas..');
			}
		});

		console.info('exibindo lista de pessoas..');
	},
	
	pageListaAnimal : function() {
		var listaDeAnimais = new AnimalCollection();
		listaDeAnimais.fetch({
			success : function() {
				var pageAnimais = new TabelaAnimais({
					collection : listaDeAnimais
				});
				$('.main-content').html(pageAnimais.render().el);
			},
			error : function() {
				console.error('Nao foi possivel carregar os animais..');
			}
		});

		console.info('exibindo lista de animais..');
	},
});
