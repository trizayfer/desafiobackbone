PageAnimais = Backbone.View.extend({

	template : templateByUrl('js/views/animal/tpl/PageAnimais.html'),

	initialize : function() {

	},

	render : function() {
		var that = this;
		// console.log(this.collection);
		this.$el.html(this.template());
		this.collection.forEach(function(animal) {
			var linha = '<tr>' + '	<td>' + animal.get('id') + '</td>' + '	<td>' + animal.get('nome') + '</td>' + '	<td>' + animal.get('idade') + '</td>' + '	<td>' + animal.get('dono') + '</td>' + '	<td> edit | delete</td>' + '</tr>';
			that.$el.find('tbody').append(linha);
		});

		return this;
	},
});