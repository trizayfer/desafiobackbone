TabelaAnimais = Backbone.View.extend({
	tagName : 'table',
	className : 'table',

	initialize : function() {
		this.collection.on('add', this.addOne, this);
		// this.collection.on('remove', this.removeOne, this);
	},

	render : function() {
		this.addHeader();

		this.collection.each(this.addOne, this);
		return this;
	},

	addHeader : function() {
		this.$el.append('<thead><td >Id</td><td >Nome</td> <td >Idade</td><td >Dono</td><td class="actions">A&ccedil;&otilde;es</td></tr></thead>');
	},

	addOne : function(animal) {
		var linhaAnimal = new LinhaAnimais({
			model : animal
		});
		this.$el.append(linhaAnimal.render().el);
	}
});
