FormAnimal = Backbone.View.extend({
	template : templateByUrl('js/views/animal/tpl/FormAnimal.html'),

	events : {
		'click #salvaAnimal' : 'salvaAnimal'
	},

	salvaAnimal : function() {
		var that = this;
		var animal = that._getModel();
		animal.save({}, {
			success : function(_model, _resp, _options) {
				console.info('Animal ' + animal.get('nome') + ' Salvo com sucesso!');
				window.location.hash = "animais";
			},

			error : function(_model, _xhr, _options) {
				console.error('Não foi possivel salvar o animal ' + pessoa.get('nome') + '!');
			}
		});
	},
	
	_getModel : function() {
		var animal = new AnimalModel({
			id : $('#inputId').val() || null,
			nome : $('#inputNome').val(),
			idade : $('#inputIdade').val(),
			dono : $('#inputDono').val(),
		});
		return animal;
	},

	initialize : function() {

	},

	render : function() {
		var toShow = this.model || new AnimalModel();
		this.$el.html(this.template(toShow.toJSON()));
		return this;
	},
});
