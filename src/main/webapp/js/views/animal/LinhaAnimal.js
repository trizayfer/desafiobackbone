LinhaAnimais = Backbone.View.extend({
	tagName : 'tr',
	template : templateByUrl('js/views/animal/tpl/TabelaAnimais.html'),

	initialize : function() {
		this.model.on('change', this.render, this);
		this.model.on('destroy', this.unrender, this);
	},

	events : {
		'click .remover' : 'removeAnimal',
	},

	render : function() {
		if (this.model && this.model.get('id')) {
			var template = this.template(this.model.toJSON());
			this.$el.html(template);
		}
		return this;
	},

	removeAnimal : function() {
		this.model.destroy();
	},

	unrender : function() {
		this.remove();
	},
});
