AnimalModel = Backbone.Model.extend({
	urlRoot : 'rs/animais',
	defaults : {
		id : null,
		nome : '',
		idade : '',
		dono : '',
	}
});