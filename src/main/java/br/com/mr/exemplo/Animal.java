package br.com.mr.exemplo;

import java.sql.Date;

public class Animal {
	private Integer id;
	private String nome;
	private String idade;
	private String dono;

	public Animal() {
		// TODO Auto-generated constructor stub
	}

	public Animal(Integer id, String nome, String idade, String dono) {
		this.id = id;
		this.nome = nome;
		this.idade = idade;
		this.dono = dono;
	}

	public String getIdade() {
		return idade;
	}

	public void setIdade(String idade) {
		this.idade = idade;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDono() {
		return dono;
	}

	public void setDono(String dono) {
		this.dono = dono;
	}
	
}
