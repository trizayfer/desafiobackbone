package br.com.mr.exemplo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/animais")
public class AnimalResources {

	private static List<Animal> ANIMAIS = new ArrayList<Animal>();//
	private static Integer idAnimais = 2;
	/*private static Pessoa dono1 = new Pessoa(5, "60017806330", "Diego");*/
	static {
		ANIMAIS.add(new Animal(1, "Toto", "5", "Diego"));
		ANIMAIS.add(new Animal(2, "Bili", "8", "David"));
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response get() {
		return Response.ok().entity(ANIMAIS).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response get(@PathParam("id") Integer id) {
		for (Animal a : ANIMAIS) {
			if (a.getId().equals(id)) {
				return Response.ok().entity(a).build();
			}
		}
		return Response.noContent().build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response salva(Animal animal) {
		animal.setId(++idAnimais);
		return Response.ok().entity(ANIMAIS.add(animal)).build();
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response update(@PathParam("id") Integer id, Animal animal) {

		for (Animal a : ANIMAIS) {
			if (a.getId().equals(id)) {
				a.setNome(animal.getNome());
				a.setIdade(animal.getIdade());
				return Response.ok().entity(a).build();
			}
		}
		return null;
	}

	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response delete(@PathParam("id") Integer id) {

		Animal toRemove = null;
		for (Animal a : ANIMAIS) {
			if (a.getId().equals(id)) {
				toRemove = a;
				break;
			}
		}
		boolean remove = ANIMAIS.remove(toRemove);
		return Response.ok().entity(remove).build();

	}
}
