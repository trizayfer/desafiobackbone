$(function() {
	// ## Funções utilitárias para utilização de templates ##
	_.templateSettings = {
		interpolate : /\{\{(.+?)\}\}/g
	};

	window.template = function(id) {
		return _.template($('#' + id).html());
	};

	window.templateByUrl = function(url) {
		return _.template(getTemplate(url));
	};

	window.getTemplate = function(url) {
		var data = "<h1> failed to load url : " + url + "</h1>";
		$.ajax({
			async : false,
			url : url,
			success : function(response) {
				data = response;
			}
		});
		return data;
	};

	var PessoaModel = Backbone.Model.extend({
		urlRoot : 'rs/pessoas',
		defaults : {
			id : null,
			nome : '',
			cpf : '',
			dataNascimento : '',
		}
	});

	var AnimalModel = Backbone.Model.extend({
		urlRoot : 'animalPlanet/animais',
		defaults : {
			id : null,
			nome : '',
			raca : '',
		}
	});

	var PessoaCollection = Backbone.Collection.extend({
		url : 'rs/pessoas',
		model : PessoaModel,
	});

	var AnimalCollection = Backbone.Collection.extend({
		url : 'animalPlanet/animais',
		model : AnimalModel,
	});

	// ////////////////////////////////////////////////////////////////////////////////
	var FormPessoa = Backbone.View.extend({
		template : templateByUrl('tpl/FormPessoa.html'),

		events : {
			'click #salvaPessoa' : 'salvaPessoa'
		},

		salvaPessoa : function() {
			var pessoaModel = this._getModel();
			pessoaModel.save({}, {
				success : function(_model, _resp, _options) {
					console.info('Pessoa ' + pessoaModel.get('nome') + ' Salvo com sucesso!');
				},

				error : function(_model, _xhr, _options) {
					console.error('Não foi possivel salvar a pessoa ' + pessoaModel.get('nome') + '!');
				}
			});
		},

		_getModel : function() {
			var pessoa = new PessoaModel({
				id : $('#inputId').val() || null,
				nome : $('#inputNome').val(),
				cpf : $('#inputCPF').val(),
				dataNascimento : $('#inputDataNascimento').val(),
			});
			return pessoa;
		},

		initialize : function() {

		},

		render : function() {
			var toShow = this.model || new PessoaModel();
			this.$el.html(this.template(toShow.toJSON()));
			return this;
		},
	});
	// //////////////////////////////////////////////////////////////////////////////
	var PagePessoas = Backbone.View.extend({

		template : templateByUrl('tpl/PagePessoas.html'),

		initialize : function() {

		},

		render : function() {
			var that = this;
			// console.log(this.collection);
			this.$el.html(this.template());
			this.collection.forEach(function(pessoa) {
				var linha = '<tr>' + '	<td>' + pessoa.get('id') + '</td>' + '	<td>' + pessoa.get('nome') + '</td>' + '	<td>' + pessoa.get('cpf') + '</td>' + '	<td>' + pessoa.get('dataNascimento') + '</td>' + '	<td> edit | delete</td>' + '</tr>';
				that.$el.find('tbody').append(linha);
			});

			return this;
		},
	});

	var LinhaPessoas = Backbone.View.extend({
		tagName : 'tr',
		template : templateByUrl('tpl/TabelaPessoas.html'),

		initialize : function() {
			this.model.on('change', this.render, this);
			this.model.on('destroy', this.unrender, this);
		},

		events : {
			'click .remover' : 'removePessoa',
		},

		render : function() {
			if (this.model && this.model.get('id')) {
				var template = this.template(this.model.toJSON());
				this.$el.html(template);
			}
			return this;
		},

		removePessoa : function() {
			this.model.destroy();
		},

		unrender : function() {
			this.remove();
		},
	});

	
	var TabelaPessoas = Backbone.View.extend({
		tagName : 'table',
		className : 'table',

		initialize : function() {
			this.collection.on('add', this.addOne, this);
//			this.collection.on('remove', this.removeOne, this);
		},

		render : function() {
			this.addHeader();

			this.collection.each(this.addOne, this);
			return this;
		},

		addHeader : function() {
			this.$el.append('<thead><td >Id</td><td >Nome</td> <td >cpf</td><td class="actions">Ações</td></tr></thead>');
		},

		addOne : function(pessoa) {
			var linhaPessoa = new LinhaPessoas({
				model : pessoa
			});
			this.$el.append(linhaPessoa.render().el);
		}
	});
	// //////////////////////////////////////////////////////////////////////////////
	var Router = Backbone.Router.extend({
		routes : {
			'' : 'entrouNaApp',
			'pessoa' : 'formNovaPessoa',
			'pessoa/:id' : 'formEditaPessoa',
			'pessoas' : 'pageListaPessoa',
		},
		entrouNaApp : function() {
			console.log('Consegui acessar a app');
		},

		formNovaPessoa : function() {
			var formPessoa = new FormPessoa();
			$('.main-content').html(formPessoa.render().el);
		},

		pageListaPessoa : function() {
			var listaDePessoas = new PessoaCollection();
			listaDePessoas.fetch({
				success : function() {
					var pagePessoas = new TabelaPessoas({
						collection : listaDePessoas
					});
					$('.main-content').html(pagePessoas.render().el);
				},
				error : function() {
					console.error('Não foi possivel carregar as pessoas..');
				}
			});

			console.info('exibindo lista de pessoas..');
		},
	});

	var r = new Router();
	Backbone.history.start();
});